﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Walking : MonoBehaviour {


    public Transform prefabStepLeft;
    public Transform prefabStepRight;
    public Transform prefabParticleStep;
    public float totalTimeLeft= 0f;
    public float totalTimeRight = 0f;

    public AudioClip left, right;
    public AudioSource AudioSRC;
    int countStep=0;
    bool startCoroutin = false;


    Transform BeforeStep;
    private void Start()
    {
                  

    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0)&&!startCoroutin)
        {
            startCoroutin = true;
            StartCoroutine("Step");
            
        }

        if (Input.GetMouseButtonUp(0) && startCoroutin)
        {
            startCoroutin = false;

            if (countStep % 2 == 0)
            {
                Instantiate(prefabStepLeft, BeforeStep.position, GetComponent<Transform>().rotation);
                //Instantiate(prefabParticleStep, GetComponent<Transform>().position+new Vector3(0,0,-5), prefabParticleStep.rotation);
                AudioSRC.PlayOneShot(left);
            }
            else if (countStep % 2 > 0)
            {
                Instantiate(prefabStepRight, BeforeStep.position, GetComponent<Transform>().rotation);

                AudioSRC.PlayOneShot(right);
            }
        }
    }
    IEnumerator Step ()
    {
        while (startCoroutin)
        {
            if (countStep % 2 == 0)
            {
                BeforeStep =  Instantiate(prefabStepLeft, GetComponent<Transform>().position, GetComponent<Transform>().rotation);
                //Instantiate(prefabParticleStep, GetComponent<Transform>().position+new Vector3(0,0,-5), prefabParticleStep.rotation);
                AudioSRC.PlayOneShot(left);
            }
            else if (countStep % 2 > 0)
            {
                BeforeStep = Instantiate(prefabStepRight, GetComponent<Transform>().position, GetComponent<Transform>().rotation);
                
                AudioSRC.PlayOneShot(right);
            }

            Instantiate(prefabParticleStep, GetComponent<Transform>().position + new Vector3(0, 0, -5), prefabParticleStep.rotation);

            countStep++;
            yield return new WaitForSeconds(1f);
        }
    }
    
}
