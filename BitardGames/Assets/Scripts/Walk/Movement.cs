﻿using System.Collections;
using UnityEngine;

public class Movement : MonoBehaviour {


    public float speedStepMovement = 2.0f;
    public Rigidbody2D rigit2dReference;
    public float distance;
    float timeSpawn = 0f;
    Vector3 mousePosition, targetPosition;
    public float offset = 0.0f;

    private void FixedUpdate()
    {
        MovementPlayer();
    }




    void MovementPlayer()
    {
        if (Input.GetMouseButton(0))
        {
            mousePosition = Input.mousePosition;
            mousePosition = Camera.main.ScreenToWorldPoint(mousePosition);
            Vector2 direction = new Vector2(mousePosition.x - transform.position.x, mousePosition.y - transform.position.y);
            transform.up = direction;
            transform.position = Vector2.Lerp(transform.position, mousePosition, Time.deltaTime/speedStepMovement);
        }

    }



}
