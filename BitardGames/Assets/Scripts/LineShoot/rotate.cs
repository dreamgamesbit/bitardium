﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class rotate : MonoBehaviour {

	private Vector3 mousePosition;
	public float moveSpeed = 2f;
	// Use this for initialization
	void Start () {
		mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);    
		transform.position = Vector2.MoveTowards(transform.position, mousePosition, moveSpeed * Time.deltaTime);

		// Тот самый поворот
		// вычисляем разницу между текущим положением и положением мыши
		Vector3 difference = mousePosition - transform.position; 
		difference.Normalize();
		// вычисляемый необходимый угол поворота
		float rotation_z = Mathf.Atan2(difference.y, difference.x) * Mathf.Rad2Deg;
		// Применяем поворот вокруг оси Z
		transform.rotation = Quaternion.Euler(0f, 0f, rotation_z);  
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
