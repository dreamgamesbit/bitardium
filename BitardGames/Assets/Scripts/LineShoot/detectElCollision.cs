﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class detectElCollision : MonoBehaviour {

	public ParticleSystem ps;
	public ParticleCollisionEvent[] collisionEvents;
	private GameObject obCollision;
    public Walking stepREference;

	// Use this for initialization
	void Start () {
		ps = GetComponent<ParticleSystem> ();
		collisionEvents = new ParticleCollisionEvent[10];
	}
	
	// Update is called once per frame
	void Update () {
		
	} 
	void OnParticleCollision(GameObject other){
        Debug.Log(other.name);
		int i = 0;
		int numCol = ps.GetCollisionEvents (other, collisionEvents);
		while (i < numCol) {
			obCollision = collisionEvents [i].colliderComponent.gameObject;
			
			if (obCollision.tag == "wall") {
                var spell = this.gameObject;
                var smain = gameObject.GetComponent<ParticleSystem>().main;
                var pnum = GetNumderByColor(smain.startColor.color);
                var bnum = GetNumderByColor(obCollision.GetComponent<colorChange>().wallColor);
                Debug.Log("партикд " + pnum + " " + "Стена " + bnum);
                if (pnum >= bnum - 30 && pnum <= bnum + 30)
                {
                    Debug.Log("Саня Чебурек");
                    Destroy(obCollision.gameObject);
                    stepREference.prefabStepLeft.GetComponent<SpriteRenderer>().color = Color.white;
                    stepREference.prefabStepRight.GetComponent<SpriteRenderer>().color = Color.white;
                }
            }
			i++;
		}
	}

    private Color GetColorByNumber(float number)
    {
        Color res = Color.blue;
        if (number < 0)
        {
            res = Color.blue;
        }
        else if (number <= 255)
        {
            res.r = 0;
            res.g = (number) / 255;
            res.b = (255 - number) / 255;
        }
        else if (number > 255 && number <= 510)
        {
            res.r = (number - 255) / 255;
            res.g = (510 - number) / 255;
            res.b = 0;
        }
        else if (number > 510)
        {
            res = Color.red;
        }
        return res;
    }
    private float GetNumderByColor(Color color)
    {
        float res = 0;
        if (color.r == 0)
        {
            res = color.g * 255;
        }
        else if (color.b == 0)
        {
            res = color.r * 255 + 255;
        }
        return res;
    }
}
