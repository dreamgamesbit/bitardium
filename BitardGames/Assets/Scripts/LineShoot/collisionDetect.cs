﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class collisionDetect : MonoBehaviour {
	public ParticleSystem ps;
	public ParticleCollisionEvent[] collisionEvents;
	private bool chekColor = false;
	private GameObject obCollision;
	float maxtimevalue = 1f;
	private float startTime;
	Color blockColor = Color.red;
	public float maxColorRange = 10;
    public Walking StepsREferece;

    // Use this for initialization
    public float speedColor=4;
	void Start () {
		ps = GetComponent<ParticleSystem>();
		collisionEvents = new ParticleCollisionEvent[100];

	}
	
	// Update is called once per frame
	void Update () {
		if (chekColor && obCollision.tag == "wall") {
			float tempTime =  Time.time - startTime;
            StepsREferece.prefabStepLeft.GetComponent<SpriteRenderer>().color = Color.Lerp (blockColor, Color.white, tempTime / speedColor);
            StepsREferece.prefabStepRight.GetComponent<SpriteRenderer>().color = Color.Lerp(blockColor, Color.white, tempTime / speedColor);
            if (tempTime / speedColor >= maxtimevalue) { 
				chekColor = false;
                StepsREferece.prefabStepLeft.GetComponent<SpriteRenderer>().color = Color.black;
                StepsREferece.prefabStepRight.GetComponent<SpriteRenderer>().color = Color.black;
                maxtimevalue = tempTime / speedColor;
			}
		}
	}

	void OnParticleCollision(GameObject other){




		int numCol = ps.GetCollisionEvents (other, collisionEvents);

		int i = 0;
		while (i < numCol){
			
			obCollision = collisionEvents [i].colliderComponent.gameObject;
			if (obCollision.tag == "wall") {

				if (!chekColor) {
                    chekColor = true;
                    startTime = Time.time;
                    float dist = Vector2.Distance(transform.position, obCollision.transform.position);
                    colorChange cl = obCollision.GetComponent<colorChange>();
                    blockColor = Color.Lerp(cl.wallColor, Color.white, GetKToColor(dist));
                    StepsREferece.prefabStepLeft.GetComponent<SpriteRenderer>().color = blockColor;
                    StepsREferece.prefabStepRight.GetComponent<SpriteRenderer>().color = blockColor;
                    //obCollision.GetComponent<Image>().color = blockColor;
                }
			}
			i++;
		}
	}
	float GetKToColor(float dist){
		float res = dist / maxColorRange;
		if (res > 1)
			res = 1;
		if (res < 0)
			res = 0;
		return res;
	}
}
