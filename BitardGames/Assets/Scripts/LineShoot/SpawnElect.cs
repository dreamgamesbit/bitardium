﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnElect : MonoBehaviour {

    public GameObject ElectPrefab;
    public GameObject ShootPrefab;
    private Vector3 spawnposition, mouseposition;
    private float distance = 1.0f;
    private float timer = 2.0f;
	private ParticleSystem[] ps;
	private ParticleCollisionEvent[] collisionEvents;
    private Vector3 mouse_pos;
    public Transform target;
    private Vector3 object_pos;
    private float angle;
	private GameObject obCollision;

    private float speed = 5.0f;
	private bool cast;
	private int castColor = 0;

    private GameObject ObjectForDestroy;
    // Use this for initialization
    void Start () {
	}
	

	// Update is called once per frame
	void Update () {

        if (Input.GetMouseButtonDown(1)) { Invoke("ElectSpawn", 0.5f); }

        if (Input.GetMouseButtonUp(1))
        {
			cast = false;
            Instantiate(ShootPrefab, ElectPrefab.transform.position, Quaternion.identity);
            mouse_pos = Input.mousePosition;
            mouse_pos.z = -20;
            object_pos = Camera.main.WorldToScreenPoint(target.position);
            mouse_pos.x = mouse_pos.x - object_pos.x;
            mouse_pos.y = mouse_pos.y - object_pos.y;
            angle = Mathf.Atan2(-mouse_pos.y, -mouse_pos.x) * Mathf.Rad2Deg;

            GameObject[] array = new GameObject[4];
            array = GameObject.FindGameObjectsWithTag("ParticalLine");
            foreach (GameObject item in array)
            {
                item.GetComponent<Transform>().rotation = Quaternion.Euler(0, 0, angle);
            }

            ObjectForDestroy = GameObject.FindGameObjectWithTag("Cast");
            GameObject.Destroy(ObjectForDestroy, 1);
            StartCoroutine(Timer(1.5f));
			ParticleSystem[] spell1 = GameObject.FindGameObjectWithTag ("Line").GetComponentsInChildren<ParticleSystem>();
			foreach (ParticleSystem item in spell1) {
				var tempMain = item.main;
				tempMain.startColor = GetColorByNumber (castColor+=30);
			}
			ps = GameObject.FindGameObjectWithTag ("Line").GetComponentsInChildren<ParticleSystem>();
			collisionEvents = new ParticleCollisionEvent[10];
        }
		if (cast) {
			ParticleSystem spell = GameObject.FindGameObjectWithTag ("spellCast").GetComponent<ParticleSystem>();
			var main = spell.main;
			main.startColor = GetColorByNumber(castColor++);
		} 
    }

    void ElectSpawn()
    {
        if (Input.GetMouseButton(1))
        {
            mouseposition = Input.mousePosition;
            spawnposition = Camera.main.ScreenToWorldPoint(new Vector3(mouseposition.x, mouseposition.y, distance));
            ElectPrefab.transform.position = spawnposition;
            Instantiate(ElectPrefab, ElectPrefab.transform.position + new Vector3(0,0,-5), Quaternion.identity);
			cast = true;
			castColor = 0;
        }
    }

    void ElectShoot(ref Vector3 position)
    {
        //mouseposition = Input.mousePosition;
        //spawnposition= Camera.main.ScreenToWorldPoint(new Vector3(mouseposition.x + Mathf.Abs(mouseposition.x*2), mouseposition.y+ Mathf.Abs(mouseposition.y*2), distance));
        //Instantiate(ShootPrefab, ShootPrefab.transform.position, Quaternion.identity);        
    }
	private Color GetColorByNumber(float number){
		Color res= Color.blue;
		if (number < 0) {
			res = Color.blue;
		} else if (number <= 255) {
			res.r = 0;
			res.g = (number)/255;
			res.b = (255 - number)/255;
		} else if (number > 255 && number <= 510) {
			res.r = (number - 255)/255;
			res.g = (510 - number)/255;
			res.b = 0;
		} else if (number > 510) {
			res = Color.red;
		}
		return res;
	}
		 
    IEnumerator Timer(float time)
    {
        YieldInstruction YieldWaitInstruction = new WaitForSeconds(time);
        while (true)
        {
            yield return YieldWaitInstruction;
            ObjectForDestroy = GameObject.FindGameObjectWithTag("Line");
            GameObject.Destroy(ObjectForDestroy, 1);
        }
    }
}
