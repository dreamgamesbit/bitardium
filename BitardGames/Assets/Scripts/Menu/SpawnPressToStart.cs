﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnPressToStart : MonoBehaviour {

    public GameObject ob;

	// Use this for initialization
	void Start () {
        ob.SetActive(false);
        StartCoroutine(Timer(16f));
    }
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKey(KeyCode.Escape))
        {
            Application.Quit();
            ob.SetActive(false);
            
        }
    }

    IEnumerator Timer(float time)
    {
        YieldInstruction YieldWaitInstruction = new WaitForSeconds(time);
        while (true)
        {
            yield return YieldWaitInstruction;
            ob.SetActive(true);
        }
    }
}
